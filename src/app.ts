/// <reference path="../defs/_references.d.ts" />

import {render} from 'react';

import Ticker = require('src/components/Ticker');
import quotes = require('src/network/Quotes');

window.addEventListener('load', () => {
  const symbols = ['AUD_CAD', 'AUD_CHF', 'EUR_USD'];
  render(Ticker({ snapshot: quotes.snapshot(symbols, quotes.create(symbols)) }), document.getElementById('app'));
}, false);
