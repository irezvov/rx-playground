/// <reference path="../../defs/_references.d.ts" />

import {createFactory, Component} from 'react';
import React = require('react');
import {Observable} from 'rx';
import RxComponent = require('src/utils/RxComponent');
import block = require('src/utils/BemSelector');
import quotes = require('src/network/Quotes');
import {List, Map} from 'immutable';

const div = createFactory('div');
const table = createFactory('table');
const tr = createFactory('tr');
const th = createFactory('th');
const td = createFactory('td');

type Snapshot = Map<string, quotes.ISnapshotQuote>;

interface CounterProps {
  snapshot: Observable<Snapshot>;
}

interface CounterState {
  snapshot: Snapshot;
}

function getTrendColor(trend: quotes.QuoteTrend) {
  switch (trend) {
    case quotes.QuoteTrend.Up:
      return 'green';
    case quotes.QuoteTrend.Neutral:
      return 'black';
    case quotes.QuoteTrend.Down:
      return 'red';
  }
}

class TikerRowClass extends Component<{ quote: quotes.ISnapshotQuote }, {}> {
  render() {
    let b = block('ticker');
    let q = this.props.quote;
    return tr({ className: b.elem('row')},
      td(null, q.symbol),
      td({ style: { color: getTrendColor(q.askTrend) } }, q.bid.toFixed(5)),
      td({ style: { color: getTrendColor(q.bidTrend) } }, q.ask.toFixed(5))
    )
  }
}

let TikerRow = createFactory(TikerRowClass);

class CounterClass extends RxComponent<CounterProps, CounterState> {
  constructor(props: CounterProps) {
    super(props);
    this.state = { snapshot: Map<string, quotes.ISnapshotQuote>() };
    this.subscribe(this.props.snapshot, this.updateSnapshot);
  }

  updateSnapshot(snapshot: Snapshot) {
    this.setState({ snapshot });
  }

  render() {
    const b = block('ticker');
    const rows = this.state.snapshot.toArray().map((q) => TikerRow({ quote: q, key: q.symbol }));
    return React.DOM.table({className: b.toString()},
      tr({ className: b.elem('head') },
        th(null, 'Symbol'),
        th(null, 'Bid'),
        th(null, 'Ask')
      ),
      React.DOM.tbody(null, rows)
    );
  }
}

export = createFactory(CounterClass);