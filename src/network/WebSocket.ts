/// <reference path="../../defs/_references.d.ts" />

import Rx from 'rx';

export enum WebSocketEvent {
  Connecting = 1,
  Connected = 2,
  Message = 3,
  Closed = 4
}

export interface WebSocketState<T> {
  event: WebSocketEvent;
  data?: T;
  error?: string;
}

export function connect<T>(url: string, protocol: string = ''): Rx.Observable<WebSocketState<T>> {
  let ws = new WebSocket(url, protocol);
  let pendingMessages = [];
  let observable = Rx.Observable.create((observer: Rx.Observer<WebSocketState<T>>) => {
    ws.onmessage = (msg: { data: any }) => {
      try {
        let data = JSON.parse(msg.data);
        observer.onNext({
          event: WebSocketEvent.Message,
          data: data
        });
      }
      catch (e) {
        observer.onError(e);
      }
    }
    ws.onerror = observer.onError.bind(observer);
    ws.onclose = observer.onCompleted.bind(observer);
    ws.onopen = () => {
      observer.onNext({ event: WebSocketEvent.Connected });
      pendingMessages.forEach(msg => ws.send(msg));
    }
  });

  let observer = Rx.Observer.create(function(data: any) {
    if (ws.readyState == WebSocket.OPEN) {
      ws.send(data);
    }
    else {
      pendingMessages.push(data);
    }
  })

  return Rx.Subject.create(observer, observable);
}
