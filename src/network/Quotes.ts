/// <reference path="../../defs/_references.d.ts" />

import {Observable, Observer} from 'rx';
import {Map} from 'immutable';

export interface IQuote {
  symbol: string;
  time: Date;
  ask: number;
  bid: number;
}

export enum QuoteTrend {
  Up,
  Neutral,
  Down
}

export interface ISnapshotQuote extends IQuote {
  askTrend: QuoteTrend;
  bidTrend: QuoteTrend;
}

export function create(symbols: string[]): Observable<IQuote> {
  return Observable.create((observer: Observer<IQuote>) => {
    let xhr = new XMLHttpRequest();
    let last = 0;
    xhr.open('GET', 'http://stream-sandbox.oanda.com/v1/prices?accountId=12345&instruments=' + symbols.join('%2C'));

    xhr.onprogress = () => {
      let msg = xhr.response.slice(last);
      last += msg.length;
      msg.split('\n')
        .filter((s) => !!s.trim())
        .map(JSON.parse)
        .filter((o) => !!o.tick)
        .forEach((o: { tick: { bid: number; ask: number; time: string; instrument: string; }}) => {
          let t = o.tick;
          observer.onNext({
            bid: t.bid,
            ask: t.ask,
            time: new Date(t.time),
            symbol: t.instrument
          })
        });
    }

    xhr.onerror = observer.onError.bind(observer);
    xhr.onload = observer.onCompleted.bind(observer);
    xhr.send();
  });
}

export function snapshot(symbols: string[], signal: Observable<IQuote>): Observable<Map<string, ISnapshotQuote>> {
  let symbolSygnals = symbols.map((symbolName) => signal.filter(q => q.symbol == symbolName));
  let state = Map<string, ISnapshotQuote>();
  return Observable.combineLatest(symbolSygnals, (...quotes) => {
    quotes.forEach((q) => {
      let old = state.get(q.symbol);
      let newQuote = (old && old.time === q.time)
        ? old
        : {
            symbol: q.symbol,
            bid: q.bid,
            ask: q.ask,
            time: q.time,
            askTrend: old && old.ask !== q.ask ? (old.ask < q.ask ? QuoteTrend.Up : QuoteTrend.Down) : QuoteTrend.Neutral,
            bidTrend: old && old.bid !== q.bid ? (old.bid < q.bid ? QuoteTrend.Up : QuoteTrend.Down) : QuoteTrend.Neutral
          };
      state = state.set(q.symbol, newQuote);
    })
    return state;
  })
}