class Bem {
  protected block: string;
  protected base: string;

  constructor(block, base) {
    this.block = block;
    this.base = base;
  }

  elem(name: string) {
    return new Bem(this.block, this.block + '__' + name);
  }

  mod(name: string, val: string) {
    return this.base + ' ' + this.base + '_' + name + '_' + val;
  }

  toString() {
    return this.base;
  }
}

function block(name): Bem {
  return new Bem(name, name);
}

export = block;