import {Component} from 'react';
import {IDisposable, Observable} from 'rx';

class RxComponent<P, S> extends Component<P, S> {
  private subscriptions: Rx.IDisposable[];
  private subQueue: any[];
  private isMounted: boolean;

  constructor(props: P) {
    super(props);
    this.subscriptions = [];
    this.subQueue = [];
    this.isMounted = false;
  }

  protected subscribe<T>(signal: Observable<T>, handler: (val: T) => any) {
    if (this.isMounted) {
      this.subscriptions.push(signal.subscribeOnNext(handler, this));
    }
    else {
      this.subQueue.push([signal, handler]);
    }
  }

  componentDidMount() {
    this.isMounted = true;
    this.subQueue.forEach((s) => {
      let signal: Observable<any> = s[0];
      let handler: (val: any) => any = s[1];
      this.subscriptions.push(signal.subscribeOnNext(handler, this));
    });
    this.subQueue = [];
  }

  componentWillUnmount() {
    this.subscriptions.forEach((s) => s.dispose());
  }
}

export = RxComponent;